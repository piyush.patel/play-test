package controllers

import models.Person
import play.api.libs.json._
import play.api.mvc.{Action, Controller}

/**
  * Created by pc on 28/03/18.
  */
class PersonController extends Controller {
  val PersonServices = services.PersonServices

  // Json Serializers
  implicit val readPerson = Json.reads[Person]

  implicit val personWrites = new Writes[Person] {
//
def writes(person: Person) = Json.obj(
  "id" -> person.id,
  "name" -> person.name,
  "children" -> person.children
)
  }

  def list = Action {
    Ok(Json.toJson(PersonServices.list))
  }

  def create = Action(parse.json) { implicit request =>
    request.body.validate[Person] match {
      case JsSuccess(newPerson, _) =>
        PersonServices.create(newPerson.name, newPerson.children) match {
          case Some(person) => Ok(Json.toJson(person))
          case None => InternalServerError
        }
      case JsError(errors) => BadRequest
    }
  }

//  def create = Action(parse.json) { implicit request =>
//    request.body.validate[Person] match {
//      case JsSuccess(newPerson, _) =>
//        PersonServices.create(newPerson.name) match {
//          case Some(person) => Ok(Json.toJson(person))
//          case None => InternalServerError
//        }
//      case JsError(errors) => BadRequest
//    }
//  }

  def detail(id:Long) = Action {
    Ok(Json.toJson(PersonServices.details(id)))
  }
}
