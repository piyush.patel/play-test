package services

/**
  * Created by pc on 28/03/18.
  */

import java.util.concurrent.atomic.AtomicLong

import models.Person

import scala.collection.concurrent.TrieMap

trait IPersonService {
  def list(): Seq[Person]
  def create(name: String, children: List[Long] = List()):Option[Person]
//  def create(name: String):Option[Person]
  def details(id: Long): Option[Person]
}

object PersonServices extends IPersonService {
  private val people = TrieMap.empty[Long, Person]
  private val seq = new AtomicLong

  // TODO: Send list with children id converted into name
  def list(): Seq[Person] = people.values.toSeq

  def create(name: String, children: List[Long]): Option[Person] = {
    val id = seq.incrementAndGet()
    val person = Person(id, name, children)
    people.put(id, person)
    Some(person)
  }

//  def create(name: String.: Option[Person] = {
//    val id = seq.incrementAndGet()
//    val person = Person(id, name)
//    people.put(id, person)
//    Some(person)
//  }

  def details(id: Long): Option[Person] = people.get(id)
}
