package models

/**
  * Created by pc on 28/03/18.
  */
case class Person (
                  var id: Long,
                  var name: String,
                  var children: List[Long]
                  )
//case class Person (
//                    id: Long,
//                    name: String
//                  )